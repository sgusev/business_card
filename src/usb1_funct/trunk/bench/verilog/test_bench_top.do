vlib work
vlog ../../rtl/verilog/*.v +incdir+../../rtl/verilog/
vlog ../../../../usb_phy/trunk/rtl/verilog/*.v +incdir+../../../../usb_phy/trunk/rtl/verilog/
vlog ../../../../generic_fifos/trunk/rtl/verilog/*.v +incdir+../../../../generic_fifos/trunk/rtl/verilog/
vlog ../../../../*.v
vlog ./test_bench_top.v +incdir+../../rtl/verilog/

vsim -novopt -t 1ns work.test

add wave -divider #USB_PHY_INTERFACE#
add wave test/u0/clk_i            
add wave test/u0/rst_i            
add wave test/u0/tx_dp            
add wave test/u0/tx_dn            
add wave test/u0/tx_oe            
add wave test/u0/rx_d             
add wave test/u0/rx_dp            
add wave test/u0/rx_dn            
add wave -divider #USB_MISC# 
add wave test/u0/phy_tx_mode      
add wave test/u0/usb_rst               
add wave -divider #UINTERRUPTS#
add wave test/u0/dropped_frame    
add wave test/u0/misaligned_frame 
add wave test/u0/crc16_err     
add wave -divider #VENDOR_FEATURES#   
add wave test/u0/v_set_int        
add wave test/u0/v_set_feature    
add wave test/u0/wValue           
add wave test/u0/wIndex           
add wave test/u0/vendor_data      
add wave -divider #USB_STATUS#   
add wave test/u0/usb_busy         
add wave test/u0/ep_sel      
add wave -divider #ENDPOINT_INTERFACE_1#        
add wave test/u0/ep1_cfg          
add wave test/u0/ep1_din          
add wave test/u0/ep1_we           
add wave test/u0/ep1_full         
add wave test/u0/ep1_dout         
add wave test/u0/ep1_re           
add wave test/u0/ep1_empty        
add wave test/u0/ep1_bf_en        
add wave test/u0/ep1_bf_size      
add wave -divider #ENDPOINT_INTERFACE_2#
add wave test/u0/ep2_cfg          
add wave test/u0/ep2_din          
add wave test/u0/ep2_we           
add wave test/u0/ep2_full         
add wave test/u0/ep2_dout         
add wave test/u0/ep2_re           
add wave test/u0/ep2_empty        
add wave test/u0/ep2_bf_en        
add wave test/u0/ep2_bf_size  
add wave -divider #ENDPOINT_INTERFACE_3#    
add wave test/u0/ep3_cfg          
add wave test/u0/ep3_din          
add wave test/u0/ep3_we           
add wave test/u0/ep3_full         
add wave test/u0/ep3_dout         
add wave test/u0/ep3_re           
add wave test/u0/ep3_empty        
add wave test/u0/ep3_bf_en        
add wave test/u0/ep3_bf_size      
add wave -divider #ENDPOINT_INTERFACE_4#
add wave test/u0/ep4_cfg          
add wave test/u0/ep4_din          
add wave test/u0/ep4_we           
add wave test/u0/ep4_full         
add wave test/u0/ep4_dout         
add wave test/u0/ep4_re           
add wave test/u0/ep4_empty        
add wave test/u0/ep4_bf_en        
add wave test/u0/ep4_bf_size      
add wave -divider #ENDPOINT_INTERFACE_5#
add wave test/u0/ep5_cfg          
add wave test/u0/ep5_din          
add wave test/u0/ep5_we           
add wave test/u0/ep5_full         
add wave test/u0/ep5_dout         
add wave test/u0/ep5_re           
add wave test/u0/ep5_empty        
add wave test/u0/ep5_bf_en        
add wave test/u0/ep5_bf_size      
add wave -divider #ENDPOINT_INTERFACE_6#
add wave test/u0/ep6_cfg          
add wave test/u0/ep6_din          
add wave test/u0/ep6_we           
add wave test/u0/ep6_full         
add wave test/u0/ep6_dout         
add wave test/u0/ep6_re           
add wave test/u0/ep6_empty        
add wave test/u0/ep6_bf_en        
add wave test/u0/ep6_bf_size      
add wave -divider #ENDPOINT_INTERFACE_7#
add wave test/u0/ep7_cfg          
add wave test/u0/ep7_din          
add wave test/u0/ep7_we           
add wave test/u0/ep7_full         
add wave test/u0/ep7_dout         
add wave test/u0/ep7_re           
add wave test/u0/ep7_empty        
add wave test/u0/ep7_bf_en        
add wave test/u0/ep7_bf_size      