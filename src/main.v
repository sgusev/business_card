// Endpoint Configuration Constants
`define IN      14'b00_001_000000000
`define OUT     14'b00_010_000000000
`define CTRL    14'b10_100_000000000
`define ISO     14'b01_000_000000000
`define BULK    14'b10_000_000000000
`define INT     14'b00_000_000000000

`define SDK_XILINX
//`define DEBUG_MODE


module main
(
 input wire clk,
 inout wire D_PLUS,
 inout wire D_MINUS
);
    wire    tx_dp;
    wire    tx_dn;
    wire    tx_oe;
    wire    rx_d;
    wire    rx_dp;
    wire    rx_dn;
   
    wire    clk_48;
    wire    rst;
   
    reg [3:0] counter_reset = 0;
    always @(posedge clk_48)
    begin
        if (counter_reset < 4'b1111)
            counter_reset <= counter_reset + 1'b1;
    end
    assign rst = counter_reset[3];
    

    reg [7:0] ep1_din;
    wire      ep1_re;
    reg       ep1_empty;
   

    // required clk = 48Mhz
    usb1_core
    core
    (
     .clk_i            (clk_48),
     .rst_i            (rst),

     // USB PHY Interface
     .tx_dp            (tx_dp), 
     .tx_dn            (tx_dn), 
     .tx_oe            (tx_oe),
     .rx_d             (rx_dp), 
     .rx_dp            (rx_dp), 
     .rx_dn            (rx_dn),

     // USB Misc
     .phy_tx_mode      (), 
     .usb_rst          (), 

     // Interrupts
     .dropped_frame    (), 
     .misaligned_frame (),
     .crc16_err        (),

     // Vendor Features
     .v_set_int        (),      //output
     .v_set_feature    (),      //output 
     .wValue           (),      //output
     .wIndex           (),      //output
     .vendor_data      (),      //input

     // USB Status
     .usb_busy         (), 
     .ep_sel           (),

     // Endpoint Interface
     .ep1_cfg          (`BULK  | `IN  | 14'd008),
     .ep1_din          (ep1_din),  
     .ep1_we           (), 
     .ep1_full         (),
     .ep1_dout         (), 
     .ep1_re           (ep1_re), 
     .ep1_empty        (ep1_empty),
     .ep1_bf_en        (), 
     .ep1_bf_size      (),

     .ep2_cfg          (),
     .ep2_din          (),  
     .ep2_we           (), 
     .ep2_full         (),
     .ep2_dout         (), 
     .ep2_re           (), 
     .ep2_empty        (),
     .ep2_bf_en        (), 
     .ep2_bf_size      (),
      
     .ep3_cfg          (),
     .ep3_din          (),  
     .ep3_we           (), 
     .ep3_full         (),
     .ep3_dout         (), 
     .ep3_re           (), 
     .ep3_empty        (),
     .ep3_bf_en        (), 
     .ep3_bf_size      (),
      
     .ep4_cfg          (),
     .ep4_din          (),  
     .ep4_we           (), 
     .ep4_full         (),
     .ep4_dout         (), 
     .ep4_re           (), 
     .ep4_empty        (),
     .ep4_bf_en        (), 
     .ep4_bf_size      (),
      
     .ep5_cfg          (),
     .ep5_din          (),  
     .ep5_we           (), 
     .ep5_full         (),
     .ep5_dout         (), 
     .ep5_re           (), 
     .ep5_empty        (),
     .ep5_bf_en        (), 
     .ep5_bf_size      (),
      
     .ep6_cfg          (),
     .ep6_din          (),
     .ep6_we           (), 
     .ep6_full         (),
     .ep6_dout         (), 
     .ep6_re           (), 
     .ep6_empty        (),
     .ep6_bf_en        (), 
     .ep6_bf_size      (),
      
     .ep7_cfg          (),
     .ep7_din          (),  
     .ep7_we           (), 
     .ep7_full         (),
     .ep7_dout         (), 
     .ep7_re           (), 
     .ep7_empty        (),
     .ep7_bf_en        (), 
     .ep7_bf_size      ()
     );

`ifdef DEBUG_MODE
    dcm_48
    dcm
    (
     .CLKIN_IN         (clk),
     .RST_IN           (0),
     .CLKFX_OUT        (clk_48),
     .CLKIN_IBUFG_OUT  (),
     .CLK0_OUT         (),
     .LOCKED_OUT       () 
     );
`else
    assign clk_48 = clk;
`endif

    always @(posedge clk_48)
    begin
        ep1_empty <= 0;
        if (ep1_re == 1'b1)
            ep1_din <= ep1_din + 1'b1;
    end

`ifdef SDK_XILINX
    IOBUF 
    #(
      .DRIVE            (12), // Specify the output drive strength
      .IOSTANDARD       ("DEFAULT"), // Specify the I/O standard
      .SLEW             ("SLOW")// Specify the output slew rate
      )
    IOBUF_inst_plus 
    (
     .O   (rx_dp),    // Buffer output
     .IO  (D_PLUS),   // Buffer inout port (connect directly to top-level port)
     .I   (tx_dp),    // Buffer input
     .T   (tx_oe)     // 3-state enable input, high=input, low=output
     );
    IOBUF
    #(
      .DRIVE            (12),        // Specify the output drive strength
      .IOSTANDARD       ("DEFAULT"), // Specify the I/O standard
      .SLEW             ("SLOW")     // Specify the output slew rate
      )
    IOBUF_inst_minus 
    (
     .O   (rx_dn),    // Buffer output
     .IO  (D_MINUS),  // Buffer inout port (connect directly to top-level port)
     .I   (tx_dn),    // Buffer input
     .T   (tx_oe)     // 3-state enable input, high=input, low=output
     );
`endif

endmodule
